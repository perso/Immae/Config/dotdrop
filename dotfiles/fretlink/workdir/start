#!/bin/bash

APP="$1"

if [ -z "$APP" ]; then
  if [ $(dirname $(pwd)) = "$HOME/workdir" ]; then
    APP=$(basename $(pwd))
  else
    echo "need an app to start"
    exit 1
  fi
fi

if [ "$APP" != "psql" ]; then
  cd $HOME/workdir/$APP
  source ../environment
else
  source ../environment
  APP=psql
fi

function start_stack() {
  stack exec $1;
}

function start_app() {
  trap 'make stop' EXIT

  if ! docker top mongo_container 2>/dev/null; then
    docker run -d --name mongo_container -p 27017:27017 --rm -v $(pwd)/appdata:/data/db mongo:3.6
    echo "Waiting until mongo is started"
    sleep 10
  fi

  make start
  make --directory=frontend/ start
}

function start_psql() {
  export PGPASSWORD=$POSTGRESQL_ADDON_PASSWORD
  psql -h $POSTGRESQL_ADDON_HOST -p $POSTGRESQL_ADDON_PORT -U $POSTGRESQL_ADDON_USER -d $POSTGRESQL_ADDON_DB
}

case "$APP" in
  "psql") start_psql;;
  "app") start_app;;
  "admin-root") start_stack server;;
  "carrier-directory") start_stack server;;
  "freight") start_stack freight-server;;
  "notifier") start_stack notifier-exe;;
  "pricer") start_stack pricer;;
  "toolbox") start_stack toolbox-api;;
esac
